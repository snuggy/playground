import tkinter as tk
from tkinter import filedialog
import os

print("Please provide the Google JSON file for authentication")
root = tk.Tk()
root.withdraw()
gcpServiceAccountLocation = filedialog.askopenfilename()

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = gcpServiceAccountLocation
print("Set Application Credential to " + os.environ["GOOGLE_APPLICATION_CREDENTIALS"])